# Jarren Worthen - Old Program Repository

This is a repository containing some of the programs that I have created for various homework assignments over the years. 
Its purpose is to allow me to access things I have written remotely and to serve as a small showcase of work I have done.
The code is open source but is not intended for use on the same assignments the code was created for, as that would be cheating.


NOTE: Much of this code has just been pulled from different sources and as such may require files not included to run properly.
The code is mainly here to help remind me how I have solved problems in the past
